### Environment Setup

#### 1.Node.js

###### 1.1 cnpm安装

- 高版本nodejs执行：npm install cnpm -g --registry=https://registry.npm.taobao.org
- 低版本nodejs先执行卸载：npm uninstall -g cnpm
- 低版本nodejs执行安装：npm install -g cnpm@6.0.0 --registry=https://registry.npm.taobao.org
- 使用cnmp -v 查看安装版本信息