## Web学习笔记

### 1. MyBatis

### 1.1 mybatis入门

#### 1.1.1 简化jdbc操作

1. 新建maven项目，导入mabatis依赖，设置pom.xml

   ```xml
   <dependency>
     <groupId>org.mybatis</groupId>
     <artifactId>mybatis</artifactId>
     <version>3.5.5</version>
   </dependency>
   ```

2. <u>resources</u>下新建`mybatis-config.xml`配置文件，进行配置

   ```xml
   <?xml version="1.0" encoding="UTF-8" ?>
   <!DOCTYPE configuration
     PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
     "https://mybatis.org/dtd/mybatis-3-config.dtd">
   <configuration>
     <environments default="development">
       <environment id="development">
         <transactionManager type="JDBC"/>
         <dataSource type="POOLED">
           <property name="driver" value="com.mysql.cj.jdbc.Driver"/>
           <property name="url" value="jdbc:mysql:///数据库名?useSSL=false"/>
           <property name="root" value="root"/>
           <property name="root" value="root"/>
         </dataSource>
       </environment>
     </environments>
     <mappers>
       <mapper resource="UserMapper.xml(映射器地址)"/>
     </mappers>
   </configuration>
   ```

3. <u>resources</u>下新建类映射xml文件`UserMapper.xml`,进行配置

   ```xml
   <?xml version="1.0" encoding="UTF-8" ?>
   <!DOCTYPE mapper
     PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
     "https://mybatis.org/dtd/mybatis-3-mapper.dtd">
   <mapper namespace="test"> //非mapper代理模式，起名无限制
     <select id="selectAll(sql执行语句代号)" resultType="查询实例的类路径名">
       select * from tb_user(table名)
     </select>
   </mapper>
   ```

4. <u>main</u>下新建`User`类，<u>java</u>下新建主程序类`MyBatisDemo`

   ```java
   public class MyBatisDemo {
       public static void main(String[] args) throws IOException {
           String resource = "mybatis-config.xml";
           InputStream inputStream = Resources.getResourceAsStream(resource);
           SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
   
           SqlSession sql = sqlSessionFactory.openSession();
           List<User> users = sql.selectList("test.selectAll");
           sout(users);
           sql.close();
       }
   }
   ```

##### 1.1.2 Mapper代理开发

1. <u>resources</u>下新建与Mapper接口同名文件夹<u>com/itheima/mapper</u>，放置`UserMapper.xml`，主文件夹新建`UserMapper`接口，更新`mybatis.config.xml`文件配置项，同时更新`UserMapper.xml`文件

   ```xml
   //更新mybatis.config.xml 映射文件路径
   <mappers>
        <mapper resource="com/itheima/mapper/UserMapper.xml"/> 
       //<package name="com.itheima.mapper"></package>
   </mappers>
   
   //更新UserMapper.xml 名空间
   <mapper namespace="com.itheima.mapper.UserMapper">
   ```

2. 使用Mapper代理方式进行数据库操作

   ```java
   public class MyBatisDemo2 {
       public static void main(String[] args) throws IOException {
           String resource = "mybatis-config.xml";
           InputStream inputStream = Resources.getResourceAsStream(resource);
           SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
           SqlSession sql = sqlSessionFactory.openSession();
           
           UserMapper userMapper = sql.getMapper(UserMapper.class);
           List<User> users = userMapper.selectAll();
           for (User user: users) {
               System.out.println(user);
           }
           sql.close();
       }
   }
   ```

3. 使用resultMap将字段与属性进行映射

   ```xml
   <resultMap id="brandResultMap" type="brand">
           <result column="brand_name" property="brandName"/>
           <result column="company_name" property="companyName"/>
       </resultMap>
   
       <select id="selectAll" resultMap="brandResultMap">
           select
               *
           from tb_brand;
       </select>
   ```

4. 按条件查询(模糊查询)

   ```java
    //BrandMapper.xml配置
      <select id="selectOnCondition" resultMap="brandResultMap">
           select * from tb_brand
           where status = #{status}
           and company_name like #{companyName}
           and brand_name like #{brandName};
       </select>
           
    //BrandMapper类方法
      ① List<Brand> selectOnCondition(@Param("status") int status, @Param("companyName") String companyName, @Param("brandName") String brandName);
      ② List<Brand> selectOnCondition(Brand brand);
      ③ List<Brand> selectOnCondition(Map map);
   
    //测试类
      ① List<Brand> brands = brandMapper.selectOnCondition(1,"%华为%","%华为%");
      ② Brand brand = new Brand();
        brand.setStatus(1);
        brand.setCompanyName("%华为%");
        brand.setBrandName("%华为%");
        List<Brand> brands1 = brandMapper.selectOnCondition(brand);
      ③ Map map = new HashMap<>();
        map.put("status", 1);
        map.put("companyName","%华为%");
        map.put("brandName","%华为%");
        List<Brand> brands1 = brandMapper.selectOnCondition(map);
   ```

5. 动态查询(条件缺省)

   ```java
   //BrandMapper.xml
    <select id="selectOnCondition" resultMap="brandResultMap">
           select * from tb_brand
           <where>
               <if test="status != null">
                   and status = #{status}
               </if>
               <if test="companyName != '' and companyName != null">
                   and company_name like #{companyName}
               </if>
               <if test="brandName != null and brandName != ''">
                   and brand_name like #{brandName};
               </if>
           </where>
    </select>
               
   //测试类
        Map map = new HashMap<>();
        //map.put("status", 1);
        //map.put("companyName","%华为%");
        map.put("brandName","%华为%");
        List<Brand> brands1 = brandMapper.selectOnCondition(map);
   ```

6. 单条件查询

   ```java
   //BrandMapper.xml
   <select id="selectOnConditionSingle" resultMap="brandResultMap">
           select *
           from tb_brand
           <where>
               <choose>
               <when test="status != null">
                   status = #{status}
               </when>
               <when test="companyName != null and companyName != ''">
                   company_name like #{companyName}
               </when>
               <when test="brandName != null and brandName != ''">
                   brand_name like #{brandName}
               </when>
               </choose>
           </where>
       </select>
   ```

7. 添加成员

   ```java
   //BrandMapper.xml
   <insert id="add" useGeneratedKeys="true" keyProperty="id">//将自增主键值返回，并赋予对象的id属性
           insert into tb_brand (brand_name, company_name, ordered, description, status)
           VALUES (#{brandName}, #{companyName}, #{ordered}, #{description}, #{status})
       </insert>
       
   //BrandMapper类方法
       void add(Brand brand);
   
   //测试类
       SqlSession sqlSession = sqlSessionFactory.openSession('autoCommi:'true);
       创建brand实例;
       brandMapper.add(brand);
       //sqlSession.commit();
   ```

8. 修改全部数据

   ```java
   //BrandMapper.xml
   <update id="update">
           update tb_brand
           <set>
               <if test="brandName != null and brandName != ''">
                   brand_name = #{brandName},
               </if>
               <if test="companyName != null and companyName != ''">
                   company_name = #{companyName},
               </if>
               <if test="ordered != null">
                   ordered = #{ordered},
               </if>
               <if test="description != null">
                   description = #{description},
               </if>
               <if test="status != null">
                   status = #{status}
               </if>
           </set>
           where
               id = #{id};
       </update>
           
   //BrandMapper类方法
       int add(Brand brand);
   
   //测试类
       创建brand实例;
       int count = brandMapper.add(brand);
   ```

9. 删除成员

   ```java
   //BrandMapper.xml
   <delete id="deleteById">//删除单个成员
           delete from tb_brand where id = #{id};
       </delete>
   
       <delete id="deleteByIds">//批量删除
           delete from tb_brand
           where id in
           <foreach collection="ids" separator="," item="id" open="(" close=")">
               #{id}
           </foreach>
       </delete>
   
    //BrandMapper类方法
        void deleteById(int id);
        void deleteByIds(@Para("ids") int[] ids);
   ```

10. @Select注解方式(适用简单sql语句)

    ```java
    //BrandMapper类方法
        @Select("select * from tb_brand where id = #{id}")
        Brand selectById(int id);
    ```

### 2. HTML

#### 2.1 HTML入门

##### 2.1.1 简单案例

```html
<html>
	<head>
	<title>HTML，快速入门</title>
	</head>
	<body>
	<font color = "red">乾坤未定，你我皆黑马</font>
	</body>
</html>
```

##### 2.1.2 基础标签

<img src="D:\JavaLessons\课程笔记\Images\HTML基础标签.png" alt="HTML基础标签" style="zoom:50%;" />

### 3. JavaWeb

#### 3.1 Tomcat

##### 3.1.1 Maven Web项目结构

![Maven-web项目结构](G:\StudyNotes\Java\Web\src\Maven-web项目结构.png)

##### 3.1.2 Web项目创建及共享包

1. 新建java项目，创建lib文件夹，右键lib设置Add as Library
2. 在项目中新建module
3. 右键module-选择Add Framework Support
4. 勾选web-application-确定
5. 选择File-Project Structure-Module，选中对应模块导入项目库文件，并在problems中对artifacts进行fix，导入lib

##### 3.1.3 Tomcat部署

1. 在部署的时候，修改application Context。然后再回到server选项卡，检查URL的值

2. 如果网址是：http://localhost:8080/pro01/ , 那么表明我们访问的是index.html。可通过<welcome-file-list>标签进行设置欢迎页(在tomcat的web.xml中设置，或者在自己项目的web.xml中设置)

3. 设置编码方式：

   ```java
    1)get请求方式(Tomcat8前)：
           String fname = request.getParameter("fname");
           //1.将字符串打散成字节数组
           byte[] bytes = fname.getBytes("ISO-8859-1");
           //2.将字节数组按照设定的编码重新组装成字符串
           fname = new String(bytes,"UTF-8");
   
    2)post请求方式(Tomcat8后无需对get请求进行处理）：
           request.setCharacterEncoding("UTF-8");
               
       注意：
           需要注意的是，设置编码(post)这一句代码必须在所有的获取参数动作之前
   ```