### Docker

#### 1.镜像操作

##### 1.1 mysql

1. 启动

   - linux启动/停止：systemctl start/stop mysqld

   - 简易启动：docker run -p 3306:3306 --name mysql -e MYSQL_ROOT_PASSWORD=root -d mysql:5.7.25

   - 持久化启动：docker run -p 3306:3306 --name mysql -v /usr/local/mysql/conf:/etc/mysql/conf.d -v /usr/local/mysql/logs:/logs -v /usr/local/mysql/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=root -d mysql:5.7.25

   
