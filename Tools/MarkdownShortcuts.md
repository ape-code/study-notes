### 1.菜单

- 文件：alt+F 
- 编辑：alt+E 
- 段落：alt+P 
- 格式：alt+O 
- 视图：alt+V 
- 主题：alt+T 
- 帮助：alt+H

### 2.编辑

- 选中当前行/句：Ctrl+L 
- 选中当前格式文本：Ctrl+E 
- 选中当前词：Ctrl+D 
- 跳转到文首：Ctrl+Home 
- 跳转到所选内容：Ctrl+J 
- 跳转到文末：Ctrl+End 
- 查找：Ctrl+F 
- 查找下一个：F3 
- 查找上一个：Shift+F3 
- 替换：Ctrl+H

### 3.段落

- 标题：Ctrl+1/2/3/4/5

- 段落：Ctrl+0

- 增大标题级别：Ctrl+

- 减少标题级别：Ctrl-

- 表格：Ctrl+T

- 代码块：Ctrl+Shift+K（```）

- 公式块：Ctrl+Shift+M

- 引用：Ctrl+Shift+Q

- 有序列表：Ctrl+Shift+[ （1.）

- 无序列表：Ctrl+Shift+]  (**·**)

- 增加缩进：Ctrl+] （Tab）

- 减少缩进：Ctrl+[ （Shift + Tab）

- 返回行首：双击Enter

- > 引用：> + 空格

---

- 分割线： --- + enter

### 4.格式

- **加粗**：Ctrl+B (** **) 
- ==高亮==：== ==
- *斜体*：Ctrl+I (*  *)
- <u>下划线</u>：Ctrl+U
- `底纹`：Ctrl+Shift+`  (``)
- ~~删除线~~：Alt+Shift+5 (~~ ~~)
- [超链接]()：Ctrl+K ([]())
- 图像：Ctrl+Shift+I (![]())
- 清除样式：Ctrl+
- 脚注[^脚注]：文字[^]

### 5.视图

- 显示/隐藏侧边栏：Ctrl+Shift+L
- 大纲视图：Ctrl+Shift+1
- 文档列表视图：Ctrl+Shift+2
- 文件树视图：Ctrl+Shift+3
- 源代码模式：Ctrl+/
- 专注模式：F8
- 打字机模式：F9
- 切换全屏：F11
- 实际大小：Ctrl+Shift+0
- 放大：Ctrl Shift +
- 缩小：Ctrl Shift -
- 应用内窗口切换：Ctrl+Tab
- 打开DevTools：Shift+F12